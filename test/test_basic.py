import unittest
from mock import MagicMock, patch, mock_open

from generator.generator import Generator, ModelLoader


class TestDomainFileGeneration(unittest.TestCase):

    test_data = """\
model:
  - name: Testobject
    attributes:
      - name: id
        type: UUID

    imports:
      - java.util.UUID

project:
  output-dir:   "/some_path/somedir"
  base-path:    "src/main/java"
  base-package: "com/mystack"
  domain:
    - rest
  package-suffix: 
    model: "domain"
  pivot:
    domain : "core"
    name-suffix: "Details"
"""
    basic_expected = [{'full_path': 'src/main/java/com/mystack/rest/domain/Testobject.java', 'content': ['package com.mystack.rest.domain;', 'import java.util.UUID;', '', 'public class Testobject {', '    private UUID id;', '    //Constructors', '    public Testobject() {', '    }', '    ', '    public Testobject(UUID id) {', '        this.id = id;', '    }', '    ', '    public Testobject(Testobject testobject) {', '        this.id = testobject.id;', '    }', '    ', '    //Getter/setters/toString/toDetails/fromDetails', '    public UUID getId() {', '        return id;', '    }', '    ', '    public void setId(UUID id) {', '        this.id = id;', '    }', '    ', '    ', '    public String toString() {', '        return "Testobject: ["+" Id: "+id+"]";', '    }', '    ', '    public TestobjectDetails toDetails() {', '        TestobjectDetails details = new TestobjectDetails();', '        details.setId(this.id);', '        return details;', '    }', '    ', '    public static Testobject fromDetails(TestobjectDetails details) {', '        Testobject user = new Testobject();', '        user.id = details.getId();', '        return user;', '    }', '    ', '    public boolean equals(Object obj) {', '        \n    if (this == obj)\n        return true;\n    if (obj == null)\n        return false;\n    if (getClass() != obj.getClass())\n        return false;\n\n    Testobject other = (Testobject) obj;', '            if (id == null) {\n        if (other.id != null)\n            return false;\n    }\n    else if (!id.equals(other.id))\n        return false;', '            return true;', '    }', '    ', '}']}]

    def setUp(self):
        print ("doing some setup")

    @patch("builtins.open", new_callable=mock_open, read_data=test_data)
    def test_basic_test(self, mock_open):
        """
        Ugly test to check against regression...
        patching the open for the file ModelLoader
        and checking the output against a saved version...
        """
        g = Generator("/dummy_file")
        g.generate_domain()

        assert(g.files == self.basic_expected)
    
    @patch("builtins.open", new_callable=mock_open, read_data=test_data)
    def test_split_line(self, mock_open):
        g = Generator("/dummy_file")
        split = 60
        data = [
                {"in": "public SomeClass(String plop, int number, String lol, SomeOtherClass attribute)",
                "expected": ["public SomeClass(String plop, int number, String lol,"," SomeOtherClass attribute)"]},
                {"in": "    public Stack(Stack stack) {",
                "expected": ["    public Stack(Stack stack) {"]}
               ]
        for d in data: 
            self.assertEqual(g._split_line(d["in"], split), d["expected"])


if __name__ == "__main__":
    unittest.main()
