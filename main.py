#!/usr/bin/env python3

from argparse import ArgumentParser
from generator.generator import Generator
from sys import exit


def get_args():
  parser = ArgumentParser(description='Generate file for Spring project domain oriented implementation')
  parser.add_argument('--action', choices=['write', 'overwrite', 'diff'], required=True,
                      help='Specify if the result should be diffed, written or overwrite existing files')
  parser.add_argument('--input-file', required=True, 
                       help='Input configuration file')
  parser.add_argument('--type', required=True, choices=['domain', 'events'],
                       help='Select which class to generate: domain or events(experimental)')


  args = parser.parse_args()
  if not args:
    exit(1)

  return args

if __name__ == "__main__":
  args = get_args()


  g = Generator(input_file=args.input_file, overwrite_file=(args.action == "overwrite"))

  if args.type == "domain": 
    g.generate_domain()
  else:
    g.generate_events()

  if args.action == "diff":
    g.print_diff()
 
  if args.action == "write" or args.action == "overwrite":
    g.save_files()
