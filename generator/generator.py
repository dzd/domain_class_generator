from yaml    import load, YAMLError
from pprint  import pprint
from os      import path, makedirs
from copy    import deepcopy
from difflib import HtmlDiff


class ModelLoader:
    """
    app config and model loader
    """

    # TODO: json schema enforcment should be used
    # to validate the input yaml

    data = {}

    # Private methods
    def _resolve_local(self, filename):
        fullpath = filename
        if filename[0] != '/':
            print("filname is not absolute")
            fullpath = "%s/%s" % (path.dirname(path.realpath(__file__)), filename)
        return fullpath

    def _raw_load(self, filename):
        with open(self._resolve_local(filename), 'r') as stream:
            try:
                data = load(stream)
                pprint(data)
                return data
            except YAMLError as exc:
                print(exc)

    def _transform_to_domain_old(self, data):
        """
        transforming the input model which is imcomplete
        to a complete model: 'computed'
        """

        data['computed_model'] = {}
        for domain in data['project']['domain']:
            data['computed_model'][domain] = []
            for obj in data['model']:
                # copy object
                o = deepcopy(obj)
                # then clear the attributes and copy in a loop
                o['attributes'] = []
                print(o['name'])
                for attribute in obj['attributes']:
                    a = deepcopy(attribute)
                    if "domain-specific" in attribute.keys():
                        if domain in attribute['domain-specific'].keys():
                            #special case if type is equal to 'None' the field must be deleted completely
                            if "type" in attribute['domain-specific'][domain].keys() and  \
                               attribute['domain-specific'][domain]['type'] == 'None':
                                print ("Ignoring attribute: %s, for domain: %s"%(attribute['name'],domain))
                                continue
                            print("updating: %s, %s"%(domain, attribute['name']))
                            a.update(attribute['domain-specific'][domain])
                        else:
                            print("deleting domain specific for : %s, %s"%(domain, attribute['name']))
                            del a["domain-specific"]
                    print("inserting: %s"%a)
                    o['attributes'].append(a)
                    print("%s: %s"%(domain, a))

                data['computed_model'][domain].append(o)
        pprint(data)
        return data


    def _transform_to_domain(self, data):
        """
        transforming the input model which is imcomplete
        to a complete model: 'computed'
        """

        data['computed_model'] = {}
        for domain in data['project']['domain']:
            data['computed_model'][domain] = []
            for obj in data['model']:
                # copy object
                o = deepcopy(obj)

                # then clear the attributes and copy in a loop
                o['attributes'] = []
                print(o['name'])
                for attribute in obj['attributes']:
                    # copying attribute for each domain
                    # and overwriting with domain specific values
                    a = deepcopy(attribute)
                    if "domain-specific" in attribute.keys():
                        if domain in attribute['domain-specific'].keys():
                            #special case if type is equal to 'None' the field must be deleted completely
                            if "type" in attribute['domain-specific'][domain].keys() and  \
                               attribute['domain-specific'][domain]['type'] == 'None':
                                print ("Ignoring attribute: %s, for domain: %s"%(attribute['name'],domain))
                                continue
                            print("updating: %s, %s"%(domain, attribute['name']))
                            a.update(attribute['domain-specific'][domain])
                        else:
                            print("deleting domain specific for : %s, %s"%(domain, attribute['name']))
                            del a["domain-specific"]
                    print("inserting: %s"%a)
                    o['attributes'].append(a)
                    print("%s: %s"%(domain, a))

                
                # adding the domain specific imports and annotation at class level
                for object_property in ['imports', 'annotate']:
                    # always creating all property to make the processing simpler in generate
                    if object_property not in o.keys():
                       o[object_property] = []

                    if 'domain-specific' in o.keys():
                        if domain in o['domain-specific'] and object_property in o['domain-specific'][domain]:
                            domain_specific_property = o['domain-specific'][domain][object_property]
                            # if not isinstance(domain_specific_property, list):
                            #     domain_specific_property = [domain_specific_property]
                            o[object_property].extend(domain_specific_property)


                data['computed_model'][domain].append(o)
        pprint(data)
        return data



    # Public methods
    def load(self, filename):
        data = self._raw_load(filename)
        return self._transform_to_domain(data)


class Generator:
    """
    code generator
    relies on configuration loaded by ModelLoader
    """
    files                 = []
    base_package_name     = "com."
    domain                = ["rest", "service", "persistence"]
    comment_constructor   = ["//Constructors"]
    comment_method        = ["//Getter/setters/toString/toDetails/fromDetails"]
    overwrite_file        = False
    generate_diff         = False
    save_file             = False

    def __init__(self, input_file, overwrite_file=False, save_file=False,
                 generate_diff=False):
        self.config = ModelLoader().load(input_file)
        self.indent_len = self.config['indent_len'] if 'indent_len' in self.config.keys() else 4
        self.line_break = self.config['line_break'] if 'line_break' in self.config.keys() else 180
        print(f"line break is : {self.line_break}")
        self.overwrite_file = overwrite_file

    def generate_domain(self):
        """
        iterate the computed model
        for each file generate:
            - package
            - imports
            - class/prototype/properties
            - class getter setter constructor and to string
            - pivot object translation
        """
        project   = self.config['project']
        base_path = project['base-path']
        for domain in self.config['computed_model'].keys():
            for obj in self.config['computed_model'][domain]:

                package_path = "%s/%s/%s"    % (project['base-package'], domain, project['package-suffix']['model'])
                package      = package_path.replace('/', '.')
                file_path    = "%s/%s.java"  % (package_path, obj['name'])
                full_path    = "%s/%s"       % (base_path, file_path)


                # print("file:%s" % full_path)
                file = {}
                imports           = []
                class_annotation  = []
                class_attributes  = []
                class_method      = []
                class_constructor = []

                for imp in obj['imports']:
                    imports.append("import {};".format(imp))

                for ann in obj['annotate']:
                    class_annotation.append("@{}".format(ann))

                for attribute in obj['attributes']:

                    if 'annotate' in attribute.keys():
                        class_attributes.append("@{}".format(attribute['annotate']))

                    class_attributes.append("private %s %s;" % (attribute['type'], attribute['name']))
                    class_method += self._gen_getters(attribute['name'], attribute['type'])

                class_method.extend(self._gen_toString(obj['name'], obj['attributes']))
                class_method.extend(self._gen_Pivot(domain, obj['name'], project['pivot']['name-suffix'], obj['attributes']))
                class_method.extend(self._gen_equals(obj['name'], obj['attributes']))
                class_constructor = self._gen_constructor(obj['name'], obj['attributes'])


                methods = []
                methods.extend(self.comment_constructor)
                methods.extend(class_constructor)
                methods.extend(self.comment_method)
                methods.extend(class_method)

                inner_class = []
                inner_class.append("package %s;"% package)
                inner_class += self._gen_class_annotated(imports=imports, classname=obj['name'], annotation=class_annotation,
                                                         attributes=class_attributes, methods=methods)
                
                file['full_path'] = full_path
                file['content']   = inner_class
                self.files.append(file)

        # pprint(self.files)

    def save_files(self):
        for file in self.files:
            print(f"saving file {file['full_path']}")
            self._write_file(file['full_path'], file['content'])

    def _write_file(self, filename, data):
        fullpath = "%s/%s"%(self.config['project']['output-dir'], filename)
        if path.exists(fullpath) and not self.overwrite_file:
            print("file: %s already exists, not overwriting"%(fullpath))
            return

        if not path.exists(path.dirname(fullpath)):
            makedirs(path.dirname(fullpath))

        with open(fullpath, 'w') as file:
            for line in data:
                file.write("%s\n"%line)

    def print_diff(self):
        differ = HtmlDiff()
        diff_output = "latest_html_diff.html"
        with open(diff_output, 'w') as html_report:
            for file in self.files:
                fullpath = "{}/{}".format(self.config['project']['output-dir'], file['full_path'])
            
                if not path.exists(fullpath):
                    print("No existing file: {}, no diff.".format(fullpath))
                    continue

                with open(fullpath, 'r') as f: 
                    html_report.write(differ.make_file(f.readlines(), file['content']))
        # if not path.exists(path.dirname(fullpath)):
        #     makedirs(path.dirname(fullpath))

        # with open(fullpath, 'w') as file:
        #     for line in data:
        #         file.write("%s\n"%line)

        print("Diff writen under: {}".format(diff_output))


############################################################################
    ## drop2 ## 
    # event service () and pivot generation)
    
    def generate_events(self):
        """
        ...
        """
        ## this could be customizable with the yaml
        action_mapping = { 'read': 'read', 
                           'create' : 'created', 
                           'delete' : 'deleted',
                           'update' : 'updated'}
        event_domain = ['core']

        project   = self.config['project']
        base_path = project['base-path']

        # do controller generation
        for domain in event_domain:
            for obj in self.config['computed_model'][domain]:
                for action in action_mapping.keys():   
                    for step in ['request', 'result']:
                        base_package_path = project['base-package']
                        base_package = base_package_path.replace("/", ".")

                        package_path = "{base_package_path}/{domain}/events/{obj}s/{step}"\
                           .format(base_package_path=base_package_path, 
                                   domain=domain, obj=obj['name'].lower(), action=action, step=step)

                        package = package_path.replace('/', '.').lower()
                        
                        if step == 'result':
                            base_class = "{action}Event".format(action=action_mapping[action].title())
                            class_name = "{obj}{base_class}".format(obj=obj['name'].title(), base_class=base_class)
                        else:
                            base_class = "{action}Event".format(action=action.title())
                            class_name = "{action}{obj}Event".format(obj=obj['name'].title(), action=action.title())

                        class_prototype = "public class {class_name} extends {base_class} {{".format(class_name=class_name, 
                                                                                            base_class=base_class)
                        
                        
                        file_path = "%s/%s.java"  % (package_path, class_name)
                        full_path = "%s/%s"       % (base_path, file_path)

                        pivot = "{obj}Details".format(obj=obj['name'].title())

                        #print("file:%s" % full_path)
                        file = {}
                        imports           = []
                        class_attributes  = []
                        class_method      = []
                        class_constructor = []

                        # Add imports not from obj but at least with details import
                        imports.append("import {base_package}.core.events.{base_class};"\
                           .format(base_package=base_package, domain=domain, base_class=base_class))
                        imports.append("import {base_package}.core.domain.{pivot};"\
                           .format(base_package=base_package, domain=domain, pivot=pivot))


                        # Attribute: event is having only a details attribute
                        class_attributes.append("private {pivot} details;".format(pivot=pivot))
                        class_method += self._gen_getters("details", pivot)

                        # class_method.extend(self._gen_toString(obj['name'], obj['attributes']))
        
                        # class_method.extend(self._gen_Pivot(domain, obj['name'], project['pivot']['name-suffix'], obj['attributes']))
                        # class_method.extend(self._gen_equals(obj['name'], obj['attributes']))
                        
                        # to be moved to a method != gen_constructor or adapat gen_constructor
                        class_constructor =  self._gen_function(prototype="public {classname}({pivot} details)".format(classname= class_name, pivot=pivot),
                                                                body= ["this.details = details;".format(pivot=pivot.lower())])

                        methods = []
                        methods.extend(class_constructor)
                        methods.extend(class_method)

                        inner_class = []
                        inner_class.append("package {};".format(package))
                        # TODO: annotation to be handled
                        inner_class += self._gen_class_with_prototype(imports, "", class_prototype, class_attributes, methods)
                
                        file['full_path'] = full_path
                        file['content']   = inner_class
                        self.files.append(file)
        pprint(self.files)

    ## Private methods ##

    # generate all attribute constructor
    def _gen_constructor(self, classname, attributes):
        inner                   = []
        attributes_list         = []
        # default values constructor
        default_inner           = []
        default_attributes_list = []
        has_default             = False
        copy_inner              = []
        for attribute in attributes:

            inner_line            = "this.{name} = {name};"              .format(name=attribute['name'])
            copy_inner_line       = "this.{name} = {copied_name}.{name};".format(name=attribute['name'], copied_name=classname.lower())
            constructor_attribute = "{type} {name}"                      .format(type=attribute['type'], name=attribute['name'])
            default_inner_line = inner_line
            default_constructor_attribute = constructor_attribute
            if "default" in attribute.keys():
                has_default = True
                default_inner_line = "this.{name} = {default_value};".format(name=attribute['name'], default_value=attribute['default'])
                default_constructor_attribute = ""

            inner.append(inner_line)
            copy_inner.append(copy_inner_line)
            attributes_list.append(constructor_attribute)
            default_inner.append(default_inner_line)
            default_attributes_list.append(default_constructor_attribute)

        #Prototypes
        prototype_args         = "public {classname}({attributes})".format(classname=classname,attributes=", ".join(attributes_list))
        default_prototype_args = "public {classname}({attributes})".format(classname=classname,attributes=", ".join(filter(None, default_attributes_list)))
        prototype_noargs       = "public {classname}()"            .format(classname=classname)
        copy_prototype         = "public {classname}({classname} {copied_name})".format(classname=classname, copied_name=classname.lower())

        result =  self._gen_function(prototype_noargs, [])
        result += self._gen_function(prototype_args, inner)
        result += self._gen_function(copy_prototype, copy_inner)

        if has_default:
            result += self._gen_function(default_prototype_args, default_inner)
        return result
        

    def _gen_getters(self, attr_name, attr_type):
        inner = ["return %s;" % (attr_name)]
        getter = self._gen_function("public %s get%s()" % (attr_type, first_up(attr_name)), inner)

        inner = ["this.%s = %s;" % (attr_name, attr_name)]
        setter = self._gen_function("public void set%s(%s %s)" % (first_up(attr_name), attr_type, attr_name), inner)

        result = []
        result= getter + setter + [""]
        #print(result)
        return result

    def _gen_toString(self, classname, attributes):
        body = ['return "%s: ["'%classname]
        for attribute in attributes:
            if "tostring" in attribute.keys() and attribute['tostring'] == False:
                continue
            body.append('" {}: " + {}'.format(attribute['name'], attribute['name']))
        body.append('"]";')
        body = " + ".join(body)
        
        prototype = "public String toString()"
        #body = ['return "%s: [%s]"' % (classname, "+".join(['"%s="+get%s'%(a['name'].capitalize(), a['name'].capitalize() for a in attributes])]))]

        return self._gen_function(prototype, [body])

    def _gen_equals(self, classname, attributes):
        #temporary here ?
        simple_type = ["boolean", "int", "float", "double", "long"]

        prototype = "public boolean equals(Object obj)"
        fix1 = """
if (this == obj)
    return true;
if (obj == null)
    return false;
if (getClass() != obj.getClass())
    return false;

{classname} other = ({classname}) obj;""".format(classname=classname)
       
        template_simple_type="""\
if ({attribute} != other.{attribute})
    return false;"""
    
        template_object="""\
if ({attribute} == null) {{
    if (other.{attribute} != null)
        return false;
}} else if (!{attribute}.equals(other.{attribute}))
    return false;"""

        end = """return true;"""
        body = []
        body.extend(fix1.format(classname=classname).split("\n"))
        for attribute in attributes:
            if attribute['type'] in simple_type:
                body.extend(template_simple_type.format(attribute=attribute['name']).split("\n"))
            else:
                body.extend(template_object.format(attribute=attribute['name']).split("\n"))

        body.extend(end.split("\n"))
        return self._gen_function(prototype, body)
 
    def _gen_Pivot(self, domain, classname, pivotname, attributes):
        pivot_classname = "%s%s"%(classname.capitalize(), pivotname.capitalize())
     
        body_to_details = ["%s details = new %s();"%(pivot_classname, pivot_classname)]
        body_from_details = ["%s user = new %s();"%(classname, classname)]
        for a in attributes:
            domain_specific_prefix = ""
            if "domain-specific" in a.keys() and "type" in a['domain-specific'][domain].keys():
                domain_specific_prefix = domain.capitalize()
            method_name = "%s%s"%(domain_specific_prefix, first_up(a['name']))
            body_from_details.append("user.%s = details.get%s();"%(a['name'], method_name))
            body_to_details.append("details.set%s(this.%s);"%(method_name, a['name']))
        body_to_details.append("return details;")
        body_from_details.append("return user;")
        
        prototype_to_details = "public %s to%s()"%(pivot_classname, pivotname)
        result = self._gen_function(prototype_to_details, body_to_details)

        prototype_from_details = "public static %s from%s(%s details)"%(classname, pivotname,pivot_classname)
        result += self._gen_function(prototype_from_details, body_from_details)

        return result


    def _gen_function(self, prototype, body):
        result = self._split_line(prototype + " {", self.line_break)
        result.extend(self._indent(body))
        result.append("}")
        result.append("")
        return result


    def _gen_class_with_prototype(self, imports, annotation, prototype, attributes, methods):

        """ 
        generating the class using a provided prototype, 
        use _gen_class(...) if you don't need to extend another class
        """ 
        result = []
        result.extend(imports)
        result.append("")
        result.extend(annotation)
        result.append(prototype)
        result.extend(self._indent(attributes))
        result.extend(self._indent(methods))
        result.append("}")
        return result

    def _gen_class_annotated(self, imports, annotation, classname, attributes, methods):
        return self._gen_class_with_prototype(imports=imports, annotation=annotation, prototype="public class %s {"%(classname),
                                              attributes=attributes, methods=methods)

    def _gen_class(self, imports, classname, attributes, methods):
        return self._gen_class_annotated(imports=imports, annotation=[], classname=classname,
                                              attributes=attributes, methods=methods)


    # indent each string in the array provided as argument with the global indentation property
    def _indent(self, lines):
        if isinstance(lines, str):
            return self._indent_map(lines)
        #print("indenting array: %s" % lines)
        return map(self._indent_map, lines)

    def _indent_map(self, line):
        return "%s%s"%(self.indent_len * " ", line)

    def _split_line(self, line, max_length=180):
        """ find the first space or comma before max_length digit not contained in quotes
        and replace it with a carriage return"""
        current_portion = line[:max_length]
        result = []
        while current_portion:
            #print(f"Current portion: {current_portion}, line: {line}")
            if current_portion == line: 
                cut_index = len(current_portion)
            else:
                last_comma = current_portion.rfind(",")
                last_column = current_portion.rfind(";")
                last_space = current_portion.rfind(" ")
                cut_index = max(max(last_comma,last_space),last_column) + 1
            result.append(current_portion[:cut_index])
            line = line[cut_index:]
            #print(f"cut_index: {cut_index}")
            current_portion = line[:max_length]
        print(f"returning: {result}")
        return result

def first_up(str):
    """
    as the fields in our yaml are already in camlCase
    we need only the first letter to be upper cased
    for example: isShared becomes IsShared
    """
    if not len(str) > 0:
        return str
    res = str[0].upper()
    if len(str) == 1:
        res

    return res + str[1:]
